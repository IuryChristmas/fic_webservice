seguradora
==========

A Symfony project created on June 29, 2017, 2:30 pm.

start container para banco de teste

```bash
docker run --name seguradora-mysql -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=seguradora -p 3310:3306 mysql:latest
```

atualizar composer

```bash
composer update
```

restaurar schema de entidades
```bash
php bin/console doctrine:schema:create
```

executar servidor

```bash
php bin/console server:run
```