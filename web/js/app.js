var appCotacao = angular.module('appCotacao', []);

appCotacao.controller('ctrlCotacao', ['$scope', '$http', function($scope, $http){
    $scope.cotacao = {};
    $scope.alert = {};
    $scope.marcas = {};
    $scope.seguradoras = {};
    $scope.valorSeguradoras = {};

    $http.get('/api/marca/all.json').then(function (result) {
        $scope.marcas = result.data;
    });

    $http.get('/api/seguradora/all.json').then(function (result) {
        $scope.seguradoras = result.data;
    });

    $scope.buscarValorSeguradoras = function (marca_id) {
        var config = {
            params: {"data_nascimento": $scope.cotacao.data_nascimento},
            headers : {'Accept' : 'application/json'}
        };

        $http.get('/api/cotacao/taxa/valorMarcasBySeguradora/'+marca_id+'.json', config).then(function (result) {
                $scope.valorSeguradoras = result.data;
                },function (result) {
                $scope.alert = {"type" : "danger", "msg" : result.data}
            });
    }
    
    $scope.adiquirirCotacao = function (cotacao, taxa) {
        cotacao.taxa = taxa.id;
        $http.post('/api/cotacao/add.json', cotacao, {headers: { 'Content-Type': 'application/json' }}).then(function (result) {
            $scope.alert = {"type" : "success", "msg" : 'Cotação feita com sucesso!'}
            $scope.cotacao = {};
            $scope.marcas = {};
            $scope.seguradoras = {};
            $scope.valorSeguradoras = {};
        },function (result) {
            $scope.alert = {"type" : "danger", "msg" : result.data}
        });
    }
}]);