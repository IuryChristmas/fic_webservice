<?php

namespace SeguradoraBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Seguradora
 *
 * @ORM\Table(name="seguradora")
 * @ORM\Entity(repositoryClass="SeguradoraBundle\Repository\SeguradoraRepository")
 */
class Seguradora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\OneToMany(targetEntity="\CotacaoBundle\Entity\Taxa", mappedBy="seguradora")
     */
    private $taxas;

    public function __construct()
    {
        $this->taxas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Seguradora
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    public function __toString()
    {
        return $this->nome;
    }
}

