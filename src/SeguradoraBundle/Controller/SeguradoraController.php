<?php

namespace SeguradoraBundle\Controller;

use SeguradoraBundle\Entity\Seguradora;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Seguradora controller.
 *
 * @Route("seguradora")
 */
class SeguradoraController extends Controller
{
    /**
     * Lists all seguradora entities.
     *
     * @Route("/", name="seguradora_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $seguradoras = $em->getRepository('SeguradoraBundle:Seguradora')->findAll();

        return $this->render('seguradora/index.html.twig', array(
            'seguradoras' => $seguradoras,
        ));
    }

    /**
     * Creates a new seguradora entity.
     *
     * @Route("/new", name="seguradora_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $seguradora = new Seguradora();
        $form = $this->createForm('SeguradoraBundle\Form\SeguradoraType', $seguradora);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($seguradora);
            $em->flush();

            return $this->redirectToRoute('seguradora_show', array('id' => $seguradora->getId()));
        }

        return $this->render('seguradora/new.html.twig', array(
            'seguradora' => $seguradora,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a seguradora entity.
     *
     * @Route("/{id}", name="seguradora_show")
     * @Method("GET")
     */
    public function showAction(Seguradora $seguradora)
    {
        $deleteForm = $this->createDeleteForm($seguradora);

        return $this->render('seguradora/show.html.twig', array(
            'seguradora' => $seguradora,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing seguradora entity.
     *
     * @Route("/{id}/edit", name="seguradora_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Seguradora $seguradora)
    {
        $deleteForm = $this->createDeleteForm($seguradora);
        $editForm = $this->createForm('SeguradoraBundle\Form\SeguradoraType', $seguradora);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seguradora_edit', array('id' => $seguradora->getId()));
        }

        return $this->render('seguradora/edit.html.twig', array(
            'seguradora' => $seguradora,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a seguradora entity.
     *
     * @Route("/{id}", name="seguradora_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Seguradora $seguradora)
    {
        $form = $this->createDeleteForm($seguradora);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($seguradora);
            $em->flush();
        }

        return $this->redirectToRoute('seguradora_index');
    }

    /**
     * Creates a form to delete a seguradora entity.
     *
     * @param Seguradora $seguradora The seguradora entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Seguradora $seguradora)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('seguradora_delete', array('id' => $seguradora->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
