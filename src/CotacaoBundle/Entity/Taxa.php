<?php

namespace CotacaoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Taxas
 *
 * @ORM\Table(name="taxa")
 * @ORM\Entity(repositoryClass="CotacaoBundle\Repository\TaxaRepository")
 */
class Taxa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="porcentagem", type="decimal", precision=10, scale=2)
     */
    private $porcentagem;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=8)
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="\MarcaBundle\Entity\Marca", inversedBy="taxas")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     */
    private $marca;

    /**
     * @ORM\ManyToOne(targetEntity="\SeguradoraBundle\Entity\Seguradora", inversedBy="taxas")
     * @ORM\JoinColumn(name="seguradora_id", referencedColumnName="id")
     */
    private $seguradora;

    /**
     * @ORM\OneToMany(targetEntity="Cotacao", mappedBy="taxa")
     */
    private $cotacoes;

    public function __construct()
    {
        $this->cotacoes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set porcentagem
     *
     * @param string $porcentagem
     *
     * @return Taxas
     */
    public function setPorcentagem($porcentagem)
    {
        $this->porcentagem = $porcentagem;

        return $this;
    }

    /**
     * Get porcentagem
     *
     * @return string
     */
    public function getPorcentagem()
    {
        return $this->porcentagem;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Taxas
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set marca
     *
     * @param \stdClass $marca
     *
     * @return Taxas
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \stdClass
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set seguradora
     *
     * @param \stdClass $seguradora
     *
     * @return Taxas
     */
    public function setSeguradora($seguradora)
    {
        $this->seguradora = $seguradora;

        return $this;
    }

    /**
     * Get seguradora
     *
     * @return \stdClass
     */
    public function getSeguradora()
    {
        return $this->seguradora;
    }

    public function __toString()
    {
        return sprintf('(%d) %s - %s, %s', $this->id, $this->porcentagem, $this->marca, $this->seguradora);
    }
}

