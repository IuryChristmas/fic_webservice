<?php
namespace CotacaoBundle\Model;

use CotacaoBundle\Entity\Taxa;

class TaxaModel
{
    private static $valorBase = 1000.00;
    
    public static function calcularTaxas($taxas, \DateTime $nascimento)
    {
        $listtaxas = [];

        /** @var Taxa $taxa */
        foreach ($taxas as $taxa)
            array_push($listtaxas, ['taxa' => $taxa, 'calculo' => self::calculoIdade($taxa, $nascimento)]);

        return $listtaxas;
    }


    /**
     *
     * De 18 a 24 anos: + 28% (aumento de 28 porcento)
     * De 25 a 32 anos: + 12% (aumento de 12 porcento)
     * De 33 a 40 anos: - 6% (diminuição de 6 porcento)
     * De 41 a 48 anos: + 2% (aumento de 2 porcento)
     * De 49 a 60 anos: + 17% (aumento de 17 porcento)
     * Acima de 60 anos: + 25% (aumento de 25 porcento)
     *
     * @param Taxa $taxa
     * @param \DateTime $nascimento
     * @return float
     * @throws \Exception
     */
    public static function calculoIdade(Taxa $taxa, \DateTime $nascimento)
    {
        $valorSeguradora = self::calculoTaxasSeguradora($taxa);
        $idadeInterval = $nascimento->diff(new \DateTime());
        $idade = $idadeInterval->y;


        if($idade < 18)
            throw new \Exception('Sua idade não pode ser menor que 18 anos');

        if($idade <= 24)
            return $valorSeguradora + ($valorSeguradora * 0.28);

        if($idade <= 32)
            return $valorSeguradora + ($valorSeguradora * 0.12);

        if($idade <= 40)
            return $valorSeguradora - ($valorSeguradora * 0.06);

        if($idade <= 48)
            return $valorSeguradora + ($valorSeguradora * 0.02);

        if($idade <= 60)
            return $valorSeguradora + ($valorSeguradora * 0.17);

        //maior que 60 anos
        return $valorSeguradora + ($valorSeguradora * 0.25);



    }

    private static function calculoTaxasSeguradora(Taxa $taxa)
    {
        $calculoBase = self::$valorBase;
        $porc = (floatval($taxa->getPorcentagem()) / 100) * $calculoBase;

        if($taxa->getTipo() == 'Positivo')
            return $calculoBase + $porc;

        return $calculoBase - $porc;
    }
}