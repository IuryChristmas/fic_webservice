<?php

namespace CotacaoBundle\Controller;

use CotacaoBundle\Entity\Taxa;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Taxa controller.
 *
 * @Route("/taxa")
 */
class TaxaController extends Controller
{
    /**
     * Lists all taxa entities.
     *
     * @Route("/", name="cotacao_taxa_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $taxas = $em->getRepository('CotacaoBundle:Taxa')->findAll();

        return $this->render('taxa/index.html.twig', array(
            'taxas' => $taxas,
        ));
    }

    /**
     * Creates a new taxa entity.
     *
     * @Route("/new", name="cotacao_taxa_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $taxa = new Taxa();
        $form = $this->createForm('CotacaoBundle\Form\TaxaType', $taxa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taxa);
            $em->flush();

            return $this->redirectToRoute('cotacao_taxa_show', array('id' => $taxa->getId()));
        }

        return $this->render('taxa/new.html.twig', array(
            'taxa' => $taxa,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a taxa entity.
     *
     * @Route("/{id}", name="cotacao_taxa_show")
     * @Method("GET")
     */
    public function showAction(Taxa $taxa)
    {
        $deleteForm = $this->createDeleteForm($taxa);

        return $this->render('taxa/show.html.twig', array(
            'taxa' => $taxa,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing taxa entity.
     *
     * @Route("/{id}/edit", name="cotacao_taxa_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Taxa $taxa)
    {
        $deleteForm = $this->createDeleteForm($taxa);
        $editForm = $this->createForm('CotacaoBundle\Form\TaxaType', $taxa);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cotacao_taxa_edit', array('id' => $taxa->getId()));
        }

        return $this->render('taxa/edit.html.twig', array(
            'taxa' => $taxa,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a taxa entity.
     *
     * @Route("/{id}", name="cotacao_taxa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Taxa $taxa)
    {
        $form = $this->createDeleteForm($taxa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($taxa);
            $em->flush();
        }

        return $this->redirectToRoute('cotacao_taxa_index');
    }

    /**
     * Creates a form to delete a taxa entity.
     *
     * @param Taxa $taxa The taxa entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Taxa $taxa)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cotacao_taxa_delete', array('id' => $taxa->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
