<?php

namespace CotacaoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="cotacao")
     */
    public function indexAction()
    {
        return $this->render('CotacaoBundle:Default:index.html.twig');
    }
}
