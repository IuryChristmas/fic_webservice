<?php

namespace CotacaoBundle\Controller;

use CotacaoBundle\Entity\Cotacao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cotacao controller.
 *
 * @Route("cotacao")
 */
class CotacaoController extends Controller
{
    /**
     * Lists all cotacao entities.
     *
     * @Route("/", name="cotacao_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cotacaos = $em->getRepository('CotacaoBundle:Cotacao')->findAll();

        return $this->render('cotacao/index.html.twig', array(
            'cotacaos' => $cotacaos,
        ));
    }

    /**
     * Creates a new cotacao entity.
     *
     * @Route("/new", name="cotacao_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cotacao = new Cotacao();
        $form = $this->createForm('CotacaoBundle\Form\CotacaoType', $cotacao);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotacao);
            $em->flush();

            return $this->redirectToRoute('cotacao_show', array('id' => $cotacao->getId()));
        }

        return $this->render('cotacao/new.html.twig', array(
            'cotacao' => $cotacao,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cotacao entity.
     *
     * @Route("/{id}", name="cotacao_show")
     * @Method("GET")
     */
    public function showAction(Cotacao $cotacao)
    {
        $deleteForm = $this->createDeleteForm($cotacao);

        return $this->render('cotacao/show.html.twig', array(
            'cotacao' => $cotacao,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cotacao entity.
     *
     * @Route("/{id}/edit", name="cotacao_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cotacao $cotacao)
    {
        $deleteForm = $this->createDeleteForm($cotacao);
        $editForm = $this->createForm('CotacaoBundle\Form\CotacaoType', $cotacao);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cotacao_edit', array('id' => $cotacao->getId()));
        }

        return $this->render('cotacao/edit.html.twig', array(
            'cotacao' => $cotacao,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cotacao entity.
     *
     * @Route("/{id}", name="cotacao_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cotacao $cotacao)
    {
        $form = $this->createDeleteForm($cotacao);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cotacao);
            $em->flush();
        }

        return $this->redirectToRoute('cotacao_index');
    }

    /**
     * Creates a form to delete a cotacao entity.
     *
     * @param Cotacao $cotacao The cotacao entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cotacao $cotacao)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cotacao_delete', array('id' => $cotacao->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
