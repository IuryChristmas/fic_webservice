<?php

namespace CotacaoBundle\Controller;

use CotacaoBundle\Entity\Cotacao;
use CotacaoBundle\Entity\Taxa;
use CotacaoBundle\Model\TaxaModel;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiController extends FOSRestController
{
    /**
     * @Route("/taxa/all.json", name="cotacao_taxa_api_index")
     * @Method("GET")
     */
    public function indexApiAction()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $vendedors = $em->getRepository('CotacaoBundle:Taxa')->findAll();
            $view = $this->view($vendedors, Response::HTTP_OK);
        } catch (\Exception $e) {
            $view = $this->view(['error' => $e->getMessage()], Response::HTTP_FORBIDDEN);
        }

        return $this->handleView($view);
    }

    /**
     * Saber o valor das taxas de cada seguradora.
     * @Route("/taxa/valorMarcasBySeguradora/{marca}.json", name="cotacao_taxa_api_index")
     * @Method("GET")
     */
    public function valorMarcasBySeguradora(Request $request, $marca)
    {
        try {
            $nascimento = new \DateTime($request->get('data_nascimento'));

            $em = $this->getDoctrine()->getManager();
            $taxas = $em->getRepository('CotacaoBundle:Taxa')->findByMarca($marca);
            $calculo = TaxaModel::calcularTaxas($taxas, $nascimento);

            $view = $this->view($calculo, Response::HTTP_OK);
        } catch (\Exception $e) {
            $view = $this->view($e->getMessage(), Response::HTTP_FORBIDDEN);
        }

        return $this->handleView($view);
    }

    /**
     * @Route("/add.json", name="cotacao_add_api_index")
     * @Method("POST")
     */
    public function addCotacao(Request $request)
    {
        $params = $request->getContent();

        $serializer = $this->container->get('serializer');

        try {
            /** @var Cotacao $cotacao */
            $cotacao = $serializer->deserialize($params, Cotacao::class, 'json');
            $cotacao->setDataNascimento(new \DateTime($cotacao->getDataNascimento()));

            $em = $this->getDoctrine()->getManager();
            $taxa = $em->getRepository('CotacaoBundle:Taxa')->find($cotacao->getTaxa());
            $cotacao->setTaxa($taxa);
            $cotacao->setValor(TaxaModel::calculoIdade($taxa, $cotacao->getDataNascimento()));
            $cotacao->setVendedor($em->getRepository('VendedorBundle:Vendedor')->find(1));
            $em->persist($cotacao);
            $em->flush();

            $view = $this->view(true, Response::HTTP_OK);
        } catch (HttpException $e) {
            $view = $this->view($e->getMessage(), $e->getStatusCode());
        } catch (\Exception $e) {
            $view = $this->view($e->getMessage(), Response::HTTP_FORBIDDEN);
        }

        return $this->handleView($view);
    }
}
