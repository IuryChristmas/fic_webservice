<?php

namespace VendedorBundle\Controller;

use VendedorBundle\Entity\Vendedor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Vendedor controller.
 *
 * @Route("vendedor")
 */
class VendedorController extends Controller
{
    /**
     * Lists all vendedor entities.
     *
     * @Route("/", name="vendedor_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vendedors = $em->getRepository('VendedorBundle:Vendedor')->findAll();

        return $this->render('vendedor/index.html.twig', array(
            'vendedors' => $vendedors,
        ));
    }

    /**
     * Creates a new vendedor entity.
     *
     * @Route("/new", name="vendedor_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $vendedor = new Vendedor();
        $form = $this->createForm('VendedorBundle\Form\VendedorType', $vendedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vendedor);
            $em->flush();

            return $this->redirectToRoute('vendedor_show', array('id' => $vendedor->getId()));
        }

        return $this->render('vendedor/new.html.twig', array(
            'vendedor' => $vendedor,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a vendedor entity.
     *
     * @Route("/{id}", name="vendedor_show")
     * @Method("GET")
     */
    public function showAction(Vendedor $vendedor)
    {
        $deleteForm = $this->createDeleteForm($vendedor);

        return $this->render('vendedor/show.html.twig', array(
            'vendedor' => $vendedor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing vendedor entity.
     *
     * @Route("/{id}/edit", name="vendedor_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vendedor $vendedor)
    {
        $deleteForm = $this->createDeleteForm($vendedor);
        $editForm = $this->createForm('VendedorBundle\Form\VendedorType', $vendedor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vendedor_edit', array('id' => $vendedor->getId()));
        }

        return $this->render('vendedor/edit.html.twig', array(
            'vendedor' => $vendedor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a vendedor entity.
     *
     * @Route("/{id}", name="vendedor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Vendedor $vendedor)
    {
        $form = $this->createDeleteForm($vendedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vendedor);
            $em->flush();
        }

        return $this->redirectToRoute('vendedor_index');
    }

    /**
     * Creates a form to delete a vendedor entity.
     *
     * @param Vendedor $vendedor The vendedor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vendedor $vendedor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vendedor_delete', array('id' => $vendedor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
