<?php

namespace VendedorBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends FOSRestController
{
    /**
     * @Route("/all.json", name="vendedor_api_index")
     * @Method("GET")
     */
    public function indexApiAction()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $vendedors = $em->getRepository('VendedorBundle:Vendedor')->findAll();
            $view = $this->view($vendedors, Response::HTTP_OK);
        } catch (\Exception $e) {
            $view = $this->view($e->getMessage(), Response::HTTP_FORBIDDEN);
        }

        return $this->handleView($view);
    }
}
